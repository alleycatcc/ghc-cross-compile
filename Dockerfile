FROM haskell:8.0.2
RUN cabal update

RUN cat /etc/apt/sources.list
RUN echo >/etc/apt/sources.list ' \
deb     http://debian.snt.utwente.nl/debian/ stretch main contrib \n\
deb-src http://debian.snt.utwente.nl/debian/ stretch main contrib \n\
'
RUN apt-get update
RUN apt-get -y install bash curl unzip apt-utils xz-utils python autoconf pkg-config libtool make texinfo

RUN git clone https://gitlab.com/alleycatcc/ghc-cross-compile
RUN cd ghc-cross-compile && git reset --hard 112596f6f2d20e0468dce21b7cf016d78431bc25
RUN cd ghc-cross-compile && git submodule update --init --recursive
RUN cd ghc-cross-compile && cp vars-docker.sh vars.sh

RUN cd ghc-cross-compile && bin/build-project --mode=get-ndk
RUN cd ghc-cross-compile && bin/build-project --mode=init-gpg
RUN cd ghc-cross-compile && bin/build-project --mode=get-clang
RUN cd ghc-cross-compile && bin/build-project --mode=get-ghc-src
RUN cd ghc-cross-compile && bin/build-project --mode=get-extra-sources
RUN cd ghc-cross-compile && bin/build-project --mode=get-ghc
RUN cd ghc-cross-compile && bin/build-project --mode=init
RUN cd ghc-cross-compile && bin/build-project --mode=build-extra-sources

RUN apt-get -y install python3
RUN apt-get -y install patch

RUN cd ghc-cross-compile && bin/build-project --mode=build-ghc
