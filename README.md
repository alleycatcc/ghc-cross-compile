# A (Dockerised) GHC cross-compiler. Targets armv7-linux-androideabi out of the box.

## To use it, either:

### 1) Use the Docker image (recommended).

    docker run --mount=type=bind,source=<your-source-dir>,destination=/host -it alleycatcc/ghc-armv7-linux-androideabi:latest /bin/bash
    # --- inside the container:
    /dist/bin/armv7-linux-androideabi-ghc /host/<your-source-file>
    
- If you're handy with Docker you can also wire the `CMD` to be the `ghc` binary.
    
### 2) Build and/or tweak the Docker image yourself.

    git submodule update --init --recursive
    bin/docker
    # --- edit Dockerfile?
    bin/docker

### 3) Build the project yourself. 

    git submodule update --init --recursive
    cp vars.sh-example vars.sh
    # --- edit the values in vars.sh
    bin/build-project
    
- This is subject to your system's quirks of course. Our builds are reproducible on an x86_64 machine running Debian testing.