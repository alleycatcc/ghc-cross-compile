#!/bin/bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")
rootdir="$bindir"/..

. "$bindir"/functions.bash
. "$bindir"/util.sh

USAGE="Usage: $0 {
      --mode=init-gpg
    | --mode=get-ndk
    | --mode=get-clang
    | --mode=get-ghc-src
    | --mode=get-extra-sources
    | --mode=get-ghc
    | --mode=init
    | --mode=build-extra-sources
    | --mode=build-ghc
}"

mode_init_gpg=
mode_get_ndk=
mode_get_clang=
mode_get_ghc_src=
mode_get_extra_sources=
mode_get_ghc=
mode_init=
mode_build_extra_sources=
mode_build_ghc=

while getopts h-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                mode=init-gpg) mode_init_gpg=yes ;;
                mode=get-ndk) mode_get_ndk=yes ;;
                mode=get-clang) mode_get_clang=yes ;;
                mode=get-ghc-src) mode_get_ghc_src=yes ;;
                mode=get-ghc) mode_get_ghc=yes ;;
                mode=get-extra-sources) mode_get_extra_sources=yes ;;
                mode=init) mode_init=yes ;;
                mode=build-extra-sources) mode_build_extra_sources=yes ;;
                mode=build-ghc) mode_build_ghc=yes ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

. "$rootdir"/vars.sh
. "$bindir"/vars-build.sh

init-gpg () {
    cmd chmod 700 "$gpgdir"
    chd "$gpgdir"
    local i
    for i in "${gpgkeys[@]}"; do
        fun gpg-cmd --import "$i".pub
    done
    chd-back
}

get-ndk () {
    local file=ndk.zip
    cmd mkdir -p "$ndkrootdir"
    chd "$ndkrootdir"
    if [ -d "$ndkstubdir" ]; then
        error "$ndkmaindir exists, exiting."
    fi
    cmd curl -o "$file" "$ndkremote"
    fun sha1sumc <<< "$ndkremotesha1 $file"
    cmd unzip "$file"
    cmd rm -f "$file"
}

get-clang () {
    local file=clang.tar.xz
    local sigfile="$file".sig

    mkd "$clangrootdir"
    chd "$clangrootdir"

    if [ -d "$clangbasedir" ]; then
        error "$clangdir exists, exiting."
    fi

    cmd curl -o "$file" "$clangremote"
    cmd curl -o "$sigfile" "$clangsigremote"
    fun gpg-verify "$sigfile" "$file"
    cmd tar Jxvf "$file"
    cmd rm -f "$file" "$sigfile"
}

get-ghc-src () {
    mkchd "$ghcrootdir"
    cmd git clone --recursive "$ghcremote" src
}

get-ffi () {
    chd "$rootdir"
    fun safe-rm-dir-allow-absolute "$ffistub"
    cmd git clone "$ffiremote" "$ffistub"
    cwd "$ffistub" cmd git reset --hard "$fficommit"
}

get-iconv () {
    local file=iconv.tar.gz
    local sigfile="$file".sig

    chd "$rootdir"
    cmd curl -Lo "$file" "$iconvremote"
    cmd curl -Lo "$sigfile" "$iconvsigremote"
    fun gpg-verify "$sigfile" "$file"
    cmd safe-rm-dir "$iconvstub"

    cmd tar zxvf "$file"
    cmd rm -f "$file" "$sigfile"
}

get-curses () {
    local file=curses.tar.gz
    local sigfile=curses.tar.gz.sig

    chd "$rootdir"

    cmd curl -Lo "$file" "$cursesremote"
    cmd curl -Lo "$sigfile" "$cursessigremote"
    fun gpg-verify "$sigfile" "$file"
    cmd safe-rm-dir "$cursesstub"
    cmd tar zxvf "$file"
    cmd rm -f "$file" "$sigfile"
}

# --- get & unpack iconv, curses, and ffi sources in the project root,
# removing them if they exist.

get-extra-sources () {
    fun get-ffi
    fun get-iconv
    fun get-curses
}

build-toolchain () {
    chd "$ndkmaindir"

    mci
    mcb build/tools/make_standalone_toolchain.py
    # --- x86 also possible.
    mcb   --arch arm
    # --- so it's idempotent.
    mcb   --force
    mcb   --api "$androidapiversion"
    mcb   --install-dir="$ndkdir"
    mcg
}

get-ghc () {
    chd "$stackdir"
    cmd stack build
}

print2 () {
    awk '{print $2}'
}

grep-compiler-exe () {
    grep compiler-exe
}

get-ghc-cmd () {
    local ret=$1; shift
    chd "$stackdir"
    pipe-n-capture _ret0 2 print2 grep-compiler-exe stack path
    chd-back

    local ghccmd="$_ret0"
    info "ghccmd: $ghccmd"
    read -d '' "$ret" <<< "$ghccmd" || true
}

init () {
    fun build-toolchain
    fun init-tool-links
}

# --- should curses become too hard to build, there is a flag to disable it
# (see ghc wiki).
build-curses () {
    chd "$cursesdir"
    if [ -f Makefile ]; then cmd make clean; fi
    # --- got flags from wavewave's nix builder, added with-shared.
    mci
    mcb ./configure
    mcb   --prefix="$cursesinstalldir"
    # --- produces warning: 'if you wanted to set the build type, don't set host' xxx
    mcb   --host="$targethost"
    mcb   --without-manpages
    mcb   --without-debug
    mcb   --without-termlib
    mcb   --without-ticlib
    mcb   --without-cxx
    mcb   --with-shared
    mcb   --enable-static=yes
    mcb   --enable-shared=yes
    mcb
    mcg

    # --- manual hack to prevent calls to 'localeconv' function, which only
    # exists in platform versions >= 21.
    cmd sed -i 's/#define HAVE_LOCALE_H 1/#define HAVE_LOCALE_H 0/' include/ncurses_cfg.h

    fun makeji

    # --- annoying hack: some files look for curses headers with the
    # ncurses/ path prefixed, others not.
    chd "$cursesinstalldir"/include/ncurses
    cmd rm -f ncurses
    cmd ln -s . ncurses
}

build-ffi () {
    chd "$ffidir"

    if [ -f Makefile ]; then cmd make clean; fi

    cmd ./autogen.sh

    mci
    mcb ./configure
    mcb   --prefix="$ffiinstalldir"
    mcb   --host="$targethost"
    mcb   --enable-static=yes
    mcb   --enable-shared=yes
    mcg

    fun makeji
}

build-iconv () {
    chd "$iconvdir"

    if [ -f Makefile ]; then cmd make clean; fi

    mci
    mcb ./configure
    mcb   --prefix="$iconvinstalldir"
    mcb   --host="$targethost"
    mcb   --enable-static=yes
    mcb   --enable-shared=yes
    mcg

    fun makeji
}

build-extra-sources () {
    fun build-curses
    fun build-ffi
    fun build-iconv
}

init-tool-links () {
    local i
    chd "$crossbindir"
    for i in ar clang clang++ ld ld.bfd ld.gold strip gcc nm cpp g++; do
        mci
        mcb ln -sf
        mcb   arm-linux-androideabi-"$i"
        mcb   armv7-linux-androideabi-"$i"
        mcg
    done
}

reset-ghc () {
    chd "$ghcsrcdir"
    cmd git reset --hard "$ghctag"
    cmd git submodule update --init --recursive
}

build-ghc-prepare () {
    local i
    for i in happy alex stack; do
        if ! quiet which "$i"; then
            error "Missing binary: $i (please install via cabal install / or your package manager; you can then resume this step)"
        fi
    done

    chd "$ghcsrcdir"
    cmd git clean -xfd
    cmd git submodule foreach --recursive git clean -xfd

    local savepath="$PATH"

    # --- for opt & llc
    xport PATH "$clangbindir":"$PATH"

    cmd ./boot

    fun get-ghc-cmd _ret0
    local ghccmd=$_ret0

    mci
    mcb ./configure --target=$targethost
    mcb   --prefix="$ghcinstalldir"
    mcb   --disable-large-address-space
    mcb   --disable-largefile
    mcb   --with-ghc="$ghccmd"
    mcb   --with-iconv-includes="$iconvinstalldir"/include
    mcb   --with-iconv-libraries="$iconvinstalldir"/lib
    mcb   --with-curses-includes="$cursesinstalldir"/include/ncurses
    mcb   --with-curses-libraries="$cursesinstalldir"/lib
    mcb   --with-system-libffi
    mcb   --with-ffi-includes="$ffiinstalldir"/include
    mcb   --with-ffi-libraries="$ffiinstalldir"/lib
    mcg

    cmd cp mk/build.mk.sample mk/build.mk
    cmd sed -Ei 's/^#(BuildFlavour[ ]+= quick-cross)$/\1/' mk/build.mk

    # --- make sure all built objects are position-independent.
    # --- some of these will be superfluous, but it's ok.
    cmd sed -Ei 's,^(CONF_CC_OPTS_STAGE0.+)$,& -fPIC,' mk/config.mk
    cmd sed -Ei 's,^(CONF_CC_OPTS_STAGE1.+)$,& -fPIC,' mk/config.mk
    cmd sed -Ei 's,^(CONF_CC_OPTS_STAGE2.+)$,& -fPIC,' mk/config.mk
    cmd sed -Ei 's,^(CONF_HC_OPTS_STAGE0.+)$,& -fPIC,' mk/config.mk
    cmd sed -Ei 's,^(CONF_HC_OPTS_STAGE1.+)$,& -fPIC,' mk/config.mk
    cmd sed -Ei 's,^(CONF_HC_OPTS_STAGE2.+)$,& -fPIC,' mk/config.mk
    cmd sed -Ei 's,^(SRC_HSC2HS_OPTS_STAGE0.+)$,& --cflag=-fPIC,' mk/config.mk
    cmd sed -Ei 's,^(SRC_HSC2HS_OPTS_STAGE1.+)$,& --cflag=-fPIC,' mk/config.mk
    cmd sed -Ei 's,^(SRC_HSC2HS_OPTS_STAGE2.+)$,& --cflag=-fPIC,' mk/config.mk

    local file
    local files=(
        configure.ac
        libraries/base/configure.ac
        libraries/unix/configure.ac
    )

	for file in "${files[@]}"; do
        cmd sed -Ei 's,^AC_SYS_LARGEFILE,#AC_SYS_LARGEFILE,' "$file"
    done

    xport PATH "$savepath"
}

patch-pdep-argmismatch () {
    chd "$ghcsrcdir"
    cmd pipe patch0 cat "$rootdir"/patch0-pdep-argmismatch
}

patch-sched-affinity () {
    chd "$ghcsrcdir"
    cmd pipe patch0 cat "$rootdir"/patch0-sched-affinity
}

build-ghc-apply-patches () {
    fun patch-pdep-argmismatch
    fun patch-sched-affinity
}

build-ghc-make () {
    local savepath="$PATH"

    # --- for opt & llc
    xport-append PATH "$clangbindir"
    cwd "$ghcsrcdir" fun makeji
    xport PATH "$savepath"
}

# --- currently always cleans and starts over.
build-ghc () {
    fun reset-ghc
    fun build-ghc-prepare
    fun build-ghc-apply-patches
    fun build-ghc-make
}

modes=(
  "$mode_init_gpg"
  "$mode_get_ndk"
  "$mode_get_clang"
  "$mode_get_ghc_src"
  "$mode_get_ghc"
  "$mode_get_extra_sources"
  "$mode_init"
  "$mode_build_extra_sources"
  "$mode_build_ghc"
)

if fun check-all-empty "${modes[@]}"; then
    error "$USAGE"
fi

if [ "$mode_init_gpg" = yes ]; then
    fun init-gpg
    exit
fi

if [ "$mode_get_ndk" = yes ]; then
    fun get-ndk
    exit
fi

if [ "$mode_get_clang" = yes ]; then
    fun get-clang
    exit
fi

if [ "$mode_get_ghc_src" = yes ]; then
    fun get-ghc-src
    exit
fi

if [ "$mode_get_ghc" = yes ]; then
    fun get-ghc
    exit
fi

if [ "$mode_get_extra_sources" = yes ]; then
    fun get-extra-sources
    exit
fi

if [ "$mode_init" = yes ]; then
    fun init
    exit
fi

if [ "$mode_build_extra_sources" = yes ]; then
    fun build-extra-sources
    exit
fi

if [ "$mode_build_ghc" = yes ]; then
    fun build-ghc
    exit
fi

exit

# --- not using.
patch-compiler-pthreads () {
    chd "$ghcsrcdir"
    cmd pipe patch0 cat "$rootdir"/patch-compiler-pthreads
}

# --- not using.
patch-ftruncate64 () {
    chd "$ghcsrcdir"
    cmd pipe patch0 cat "$rootdir"/patch0-ftruncate64
}
