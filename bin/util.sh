set -eu
set -o pipefail

makei () {
    cmd make install
}

makej () {
    cmd make -j
}

makeji () {
    cmd makej
    cmd makei
}

sha1sumc () {
    sha1sum -c
}

gpg-cmd () {
    cmd gpg --homedir="$gpgdir" "$@"
}

gpg-verify () {
    fun gpg-cmd --verify "$@"
}

check-all-empty () {
    local i
    for i in "$@"; do
        if [ -n "$i" ]; then
            return 1
        fi
    done
    return 0
}

patch0 () {
    patch -p0 "$@"
}

# --- not used; possibly useful -- slightly different from
# make_standalone_toolchain.py.

make-ndk-toolchain () {
    chd "$ndkdir"
    cmd build/tools/make-standalone-toolchain.sh \
        --arch=x86 \
        --platform=android-"$androidtoolsapiversion" \
        --install-dir="$ndktoolchainx86dir"
    cmd build/tools/make-standalone-toolchain.sh \
        --arch=arm\
        --platform=android-"$androidtoolsapiversion" \
        --install-dir="$ndktoolchainarmdir"
}

