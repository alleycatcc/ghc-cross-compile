set -eu
set -o pipefail

rootdir="$bindir"/..
toolsdir="$rootdir"/tools
gpgdir="$rootdir"/gpg
stackdir="$rootdir"/stack

androidapiversion=17

ndkstubdir=android-ndk-r16b
ndkmaindir="$ndkrootdir"/"$ndkstubdir"
ndkdir="$ndkrootdir"/toolchain-r16b-"$androidapiversion"

crossbindir="$ndkdir"/bin
targethostv7=armv7-linux-androideabi
targethostplain=arm-linux-androideabi
targethost=$targethostv7

gpgkeys=(
    2E4616C2
    345AD05D
    F7E48EDB
)

ndkremote=https://dl.google.com/android/repository/"$ndkstubdir"-linux-x86_64.zip
ndkremotesha1=42aa43aae89a50d1c66c3f9fdecd676936da6128

androidsysroot="$ndkdir"/platforms/android-$androidapiversion/arch-arm

# --- clang does not provide https urls unfortunately.

# --- the build is very sensitive to clang version -- 5.0.0 works.
clangremote5_0=http://releases.llvm.org/5.0.0/clang+llvm-5.0.0-x86_64-linux-gnu-debian8.tar.xz
clangsigremote5_0=http://releases.llvm.org/5.0.0/clang+llvm-5.0.0-x86_64-linux-gnu-debian8.tar.xz.sig

clangremote="$clangremote5_0"
clangsigremote="$clangsigremote5_0"

clangbasedir=$(basename -s .tar.xz "$clangremote")
clangdir="$clangrootdir"/"$clangbasedir"
clangbindir="$clangdir"/bin

ghcsrcdir="$ghcrootdir"/src
ghcbuilddir="$ghcrootdir"/build
ghcremote=http://git.haskell.org/ghc.git

ghctag=606bbc310654fcf56fe068905bb1aca30d2f0a8a

ffiremote=https://github.com/libffi/libffi.git
ffistub=libffi
ffidir="$rootdir"/"$ffistub"
fficommit=dca52b55bc2ac0213c20849d7e9e19fbc9202023

iconvstub=libiconv-1.15
iconvremote=https://ftp.gnu.org/pub/gnu/libiconv/"$iconvstub".tar.gz
iconvsigremote=https://ftp.gnu.org/pub/gnu/libiconv/"$iconvstub".tar.gz.sig
iconvdir="$rootdir"/"$iconvstub"

cursesver=6.0
cursesremote=https://ftp.gnu.org/pub/gnu/ncurses/ncurses-"$cursesver".tar.gz
cursessigremote=https://ftp.gnu.org/pub/gnu/ncurses/ncurses-"$cursesver".tar.gz.sig
cursesstub=ncurses-"$cursesver"
cursesdir="$rootdir"/"$cursesstub"
cursesinstalldir="$cursesdir"/"$targethost"

xport PATH $PATH:"$crossbindir"
xport AR $targethost-ar
xport AS $targethost-clang
xport CC $targethost-clang
xport CXX $targethost-clang++
xport LD $targethost-ld
xport STRIP $targethost-strip

# ex ft=sh
