set -eu
set -o pipefail

# --- where to install the Android ndk.
ndkrootdir=/android/android-ndk
# --- where to install clang.
clangrootdir=/clang
# --- where to build the cross-compiler.
ghcrootdir=/ghc
# --- where to install the cross-compiler.
ghcinstalldir=/dist
# --- where to install cross-compiled extra libs.
ffiinstalldir="$ghcinstalldir"/armv7-linux-androideabi
iconvinstalldir="$ghcinstalldir"/armv7-linux-androideabi
